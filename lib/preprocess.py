import nltk
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords


def pre_process_data(input_file:str, output_file:str) -> list:
    '''Used to pre-process the input data
       会返回list格式，处理好的文本数据，并将输出写入到output_file中

    Args:
        input_file (str): Path to the input file     eg: './test.txt'
        output_file (str): Path to the output file   eg: './test_processed.txt'
        

    Returns:
        res_list: list of pre-processed data

    example:
        input_file = './test.txt'
        output_file = './test_processed.txt'
        res_list = pre_process_data(input_file, output_file)
        
    '''

    # Read the input file
    with open(input_file, 'r') as f:
        input_str_list = f.readlines()



    # 1. remove line break
    input_str_list = [line.replace('\n', '') for line in input_str_list]

    # 2. tokenize
    input_str_list = [word_tokenize(line) for line in input_str_list]

    # 3. lower case
    input_str_list = [[word.lower() for word in line] for line in input_str_list]

    # 4. remove stop words
    stop_words = set(stopwords.words('english'))
    input_str_list = [[word for word in line if word not in stop_words] for line in input_str_list]

    # 5. remove punctuation
    input_str_list = [[word for word in line if word.isalpha()] for line in input_str_list]

    # 6. remove empty line and line with only one word
    input_str_list = [line for line in input_str_list if len(line) > 1]

    # 7. stem words 词干提取
    stemmer = nltk.stem.PorterStemmer()
    input_str_list = [[stemmer.stem(word) for word in line] for line in input_str_list]

    # 8. 将list of list转换为list
    res_list = []
    for line in input_str_list:
        for word in line:
            res_list.append(word)


    # 9. write to output file
    with open(output_file, 'w') as f:
        for word in res_list:
            f.write(word + '\n')
        
    return res_list




if __name__ == "__main__":
    input_file = './test.txt'
    output_file = './test_output.txt'
    pre_process_data(input_file, output_file)
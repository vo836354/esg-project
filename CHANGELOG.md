# Changelog

## [Unreleased]

## [0.1.2] - 2022-12-3


## [0.1.1] - 2022-11-5
### Added
- 将2022/11/4新增的数据添加到[Dataset](./Dataset/SampleData/)中
- 新增[lib文件夹](./lib/)，用于存放独立出来的函数

### Changed
- 给[pdf转换方法](./Process/DataClean/PDFtoTXT/PDFtoTXT.ipynb)添加了新的输出逻辑
- 结合Google中的命名方法，重新更新[Dataset](./Dataset/SampleData/)中，子文件夹名称

### Removed


## [0.1.0] - 2022-11-4
### Added
- 所有原始数据文件(/Dataset/)更新至2022/11/4
- 完成PDF转换txt的方法
- 初步完成LDA预测，预测有待开发

### Changed


### Removed
